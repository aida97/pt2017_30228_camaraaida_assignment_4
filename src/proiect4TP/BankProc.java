package proiect4TP;

import java.util.List;

public interface BankProc {

	/**
	 * 
	 * @pre person!=null
	 * @pre banca.containsKey(person)==false
	 * @post banca.size()==size+1
	 * @post banca.containsKey(person)==true
	 * @invariant corect()
	 */
	public void addPerson(Person person);
	
	
	
	
	/**
	 * 
	 *  @pre person!=null;
	 	@pre banca.containsKey(person)==true
		@post banca.size()==size-1
		@post banca.containsKey(person)==false
		@invariant corect()
		
	 */
	public void removePerson(Person person);
	
	//public void addSavingAccount(Person person, SavingAccount account);
	
	
	
	
	
	
	
	/**
	 * 
	 *@pre person!=null
	 *@pre account!=null
	 *@pre banca.containsKey(person)==true
	 *@pre banca.containsValue(account)==false
	 *@post banca.size()==size+1
	 *@post banca.containsValue(account)==true
	 *@invariant corect()
	 */
	
	public void addAccount(Person person, Account account);
	
	
	
	
	
	
	
	/**
	 * @pre person!=null
	 * @pre banca.containsKey(person)==true
	 * @post banca.get(person).size()==0
	 * @post banca.containsKey(person)==true
	 * @invariant corect()
	 */
	public void removeAccounts(Person person);
	
	
	
	
	
	
	/**
	 * @pre person!=null
	 * @pre account!=null
	 * @pre banca.containsKey(person)==true
	 * @pre banca.containsValue(account)==true
	 * @post banca.get(person).size()==size-1
	 * @post banca.containsValue(account)==false
	 * @invariant corect()
	 */
	public void removeAccount(Person person, Account account);
	
	//public void depune(Person person, Account account);
	//public void retrage(Person person, Account account);
	
	//public void editPerson(Person person, String newName);
	
	public List<Person> allPersons();
	public List<Account> allAccounts();
	
	public boolean corect();
	
	public void serialize();
	public void deserialize();
}
