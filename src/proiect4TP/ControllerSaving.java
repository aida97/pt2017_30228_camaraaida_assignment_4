package proiect4TP;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;

public class ControllerSaving {

	@FXML
	private TextField sumaDepusa;
	
	@FXML
	private ComboBox<Person> persoaneCB;
	
	@FXML
	private TextField idCont;
	
	@FXML
	private TextField data;
	
	@FXML
	private TextField dataCrearii;
	
	@FXML
	private TextField dobanda;
	
	
	public ControllerSaving() {

	}

	@FXML
	private void initialize() {
		
	}
	
	@FXML
	private void setComboBox(){
		List<Person> persoane=Service.viewAllPersons();
		
        List<Person> persoaneAL=new ArrayList<Person>();
        for(Person persoana: persoane){
			persoaneAL.add(persoana);
		}
        
        ObservableList<Person> data = FXCollections.observableList(persoaneAL);
        
        persoaneCB.setEditable(false);
        persoaneCB.setItems(data);
	}
	
	public void addNew(){
		Person person=persoaneCB.getValue();
		String dobandaString=dobanda.getText();
		//String dataString=data.getText();
		String dataCreariiString=dataCrearii.getText();
		Service.addNewSavingAcc(person,dobandaString,dataCreariiString);
	}
	
	public void deleteAll(){
		Person person=persoaneCB.getValue();
		Service.deleteAll(person);
	}
	
	public void deleteAccount(){
		Person person=persoaneCB.getValue();
		String idContString=idCont.getText();
		Service.deleteSavingAccount(person,idContString);

	}
	public void retrage(){
		Person person=persoaneCB.getValue();
		String idString=idCont.getText();
		String dataString=data.getText();
		
		Service.retrageSaving(person, idString,dataString);
	}
	
	public void depune(){
		Person person=persoaneCB.getValue();
		String sumaString=sumaDepusa.getText();
		String idString=idCont.getText();
		String dataString=data.getText();
		
		Service.depuneSaving(person, idString, sumaString,dataString);
	}
	
	public void viewAll(){
		Stage stage = new Stage();
		Group root=new Group();
		Scene scene = new Scene(root);
        stage.setTitle("Conturi");
        stage.setWidth(800);
        stage.setHeight(500);
 
        
        GridPane panel=new GridPane();
        List<Account> conturi=Service.viewAllAccounts();
        List<Account> conturiAL=new ArrayList<Account>();
        for(Account cont: conturi){
			conturiAL.add(cont);
		}
        
         
        
        ObservableList<Account> data = FXCollections.observableList(conturiAL);
        TableView<Account> table = new TableView<Account>();
        table.setMinWidth(700);
        table.setItems(data);
     
        TableColumn<Account,String> numeTitularCol=new TableColumn<Account,String>("Nume Titular");
        TableColumn<Account,String> idTitularCol=new TableColumn<Account,String>("ID Titular");
        
        TableColumn<Account,String> idContCol=new TableColumn<Account,String>("ID Cont");
        TableColumn<Account,String> soldContCol=new TableColumn<Account,String>("Sold Cont");
        TableColumn<Account,String> dobandaContCol=new TableColumn<Account,String>("Dobanda Cont");
       // TableColumn<Account,String> dataCol=new TableColumn<Account,String>("Data");
        TableColumn<Account,String> dataCreariiCol=new TableColumn<Account,String>("Data Crearii Cont");
        
        numeTitularCol.setCellValueFactory(new Callback<CellDataFeatures<Account, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(CellDataFeatures<Account, String> p) {
                // p.getValue() returns the Person instance for a particular TableView row
                return p.getValue().getNumeP();
            }
         });
        
        idTitularCol.setCellValueFactory(new Callback<CellDataFeatures<Account, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(CellDataFeatures<Account, String> p) {
                // p.getValue() returns the Person instance for a particular TableView row
            	
                return p.getValue().getIdTP();
            }
         });
        
        idContCol.setCellValueFactory(new Callback<CellDataFeatures<Account, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(CellDataFeatures<Account, String> p) {
                // p.getValue() returns the Person instance for a particular TableView row
                return p.getValue().getIdP();
            }
         });
        
        soldContCol.setCellValueFactory(new Callback<CellDataFeatures<Account, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(CellDataFeatures<Account, String> p) {
                // p.getValue() returns the Person instance for a particular TableView row
                return p.getValue().getSoldP();
            }
         });
        
        dobandaContCol.setCellValueFactory(new Callback<CellDataFeatures<Account, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(CellDataFeatures<Account, String> p) {
                // p.getValue() returns the Person instance for a particular TableView row
            	if(p.getValue() instanceof SavingAccount){
            		 return ((SavingAccount) p.getValue()).getDobandaP();
            	}
            	else {
            		SimpleStringProperty nuAvem=new SimpleStringProperty("cont Spending");
            		return nuAvem;
            	}
            }
         });
        /*
         * 
        
        dataCol.setCellValueFactory(new Callback<CellDataFeatures<Account, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(CellDataFeatures<Account, String> p) {
                // p.getValue() returns the Person instance for a particular TableView row
            	if(p.getValue() instanceof SavingAccount){
            		return ((SavingAccount) p.getValue()).getDataP();
            	}
            	else{
            		SimpleStringProperty nuAvem=new SimpleStringProperty("cont Spending");
            		return nuAvem;
            	}
            }
         });
         */
        dataCreariiCol.setCellValueFactory(new Callback<CellDataFeatures<Account, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(CellDataFeatures<Account, String> p) {
                // p.getValue() returns the Person instance for a particular TableView row
            	if(p.getValue() instanceof SavingAccount){
            		return ((SavingAccount) p.getValue()).getDataCrP();
            	}
            	else{
            		SimpleStringProperty nuAvem=new SimpleStringProperty("cont Spending");
            		return nuAvem;
            	}
            }
         });


	table.getColumns().addAll(numeTitularCol,idTitularCol,idContCol, soldContCol, dobandaContCol, dataCreariiCol);

	root.getChildren().add(panel);
        root.getChildren().addAll(table);
        
        
        stage.setScene(scene);
        stage.show();
	}
}
