package proiect4TP;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class ControllerAccount {

	public ControllerAccount() {

	}

	@FXML
	private void initialize() {
		
	}
	@FXML
	public void spendingNewWindow(){
		FXMLLoader loader = new FXMLLoader();
		Stage stage = new Stage();

		String fxmlDocPath = "C:/Users/Aida/Desktop/fxmlfiles/spending.fxml";
		FileInputStream fxmlStream;
		try {
			fxmlStream = new FileInputStream(fxmlDocPath);
			AnchorPane root = (AnchorPane) loader.load(fxmlStream);
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.setTitle("SPENDING ACCOUNT");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}
		
		stage.show();
	}
	
	@FXML
	public void savingNewWindow(){
		FXMLLoader loader = new FXMLLoader();
		Stage stage = new Stage();

		String fxmlDocPath = "C:/Users/Aida/Desktop/fxmlfiles/saving.fxml";
		FileInputStream fxmlStream;
		try {
			fxmlStream = new FileInputStream(fxmlDocPath);
			AnchorPane root = (AnchorPane) loader.load(fxmlStream);
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.setTitle("SAVING ACCOUNT");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}
		
		stage.show();
	}
}
