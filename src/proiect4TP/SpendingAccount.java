package proiect4TP;

import java.io.Serializable;

public class SpendingAccount extends Account implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SpendingAccount() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SpendingAccount(Person titular, double sold, String id) {
		super(titular, sold,id);
		// TODO Auto-generated constructor stub
	}

	public void retrage(int suma){
		if(suma>getSold()){
			System.out.println("NU AI ASA DE MULTI BANI!!");
			
		}
		else{
			setSold(getSold()-suma);
			System.out.println("Tranzactie reusita! Sold curent: "+ getSold()+"\n");
		}
	}
	
	public void depune(int suma){
		setSold(getSold()+suma);
		System.out.println("Tranzactie reusita! Sold curent: "+getSold()+"\n");
	}
	
	
	
	
	public String toString(){
		return "ID: "+getId()+"Titular: "+getTitular().toString()+"\n"+"Sold: "+getSold()+"\n";
	}

}
