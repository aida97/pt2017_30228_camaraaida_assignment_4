package proiect4TP;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javafx.beans.property.SimpleStringProperty;

public class SavingAccount extends Account implements Serializable{
	
	
	private static final long serialVersionUID = 1L;
	private double dobanda;
	//private Calendar data;
	private Calendar dataCrearii;
	
	public SavingAccount() {
		super();
		dobanda=-1;
		//data=new GregorianCalendar();
		dataCrearii=new GregorianCalendar();
		// TODO Auto-generated constructor stub
	}

	public SavingAccount(Person titular, double sold,String id, double dobanda, Calendar dataCrearii) {
		super(titular, sold, id);
		this.dobanda=dobanda;
		//this.data=data;
		this.dataCrearii=dataCrearii;
		// TODO Auto-generated constructor stub
	}
	
	public SavingAccount(Person titular, String id){
		super(titular, id);
		this.dobanda=-1;
		dataCrearii=new GregorianCalendar();
	}
	public double getDobanda() {
		return dobanda;
	}

	public void setDobanda(double dobanda) {
		this.dobanda = dobanda;
	}
	
	//public Calendar getData(){
	//	return data;
	//}
	
	//public void setData(Calendar data){
	//	this.data=data;
	//}
	
	public Calendar getDataCrearii(){
		return dataCrearii;
	}
	
	public void setDataCrearii(Calendar data){
		this.dataCrearii=data;
	}
	

	private boolean sePoate( Calendar data){
		if(data.get(Calendar.YEAR)-dataCrearii.get(Calendar.YEAR)>=1){
			return true;
		}
		return false;
	}
	
	public void depune(int suma, Calendar data){
		double sumaNoua=(data.get(Calendar.YEAR)-dataCrearii.get(Calendar.YEAR))*dobanda*getSold()+getSold();
		double dobandaCalculata=(data.get(Calendar.YEAR)-dataCrearii.get(Calendar.YEAR))*getSold()*dobanda;
		System.out.println("La contul "+getId()+" se adauga dobanda "+ dobandaCalculata );
		dataCrearii=data;
		setSold(sumaNoua+suma);
		System.out.println("Tranzactie reusita! Soldul contului: "+getSold());
	}
	
	public void retrage( Calendar data){
		if(sePoate(data)){
			setSold((data.get(Calendar.YEAR)-dataCrearii.get(Calendar.YEAR))*dobanda*getSold()+getSold());
			System.out.println("Aveti "+getSold()+"bani dupa adunarea dobanzii acumulate");
			setSold(0);
			//dataCrearii=data;
		}
		else{
			System.out.println("nu e data potrivita ca sa scoti bani");
		}
	}
	
	public SimpleStringProperty getDobandaP(){
		SimpleStringProperty dobandaP=new SimpleStringProperty(Double.toString(dobanda));
		return dobandaP;
	}
	
	//public SimpleStringProperty getDataP(){
		//SimpleStringProperty dataP=new SimpleStringProperty(data.toString());
		//return dataP;
	//}
	
	public SimpleStringProperty getDataCrP(){
		String dCstr=Integer.toString(dataCrearii.get(Calendar.DATE))+" "+Integer.toString(dataCrearii.get(Calendar.MONTH)+1)+" "+Integer.toString(dataCrearii.get(Calendar.YEAR));
		SimpleStringProperty dataCrP=new SimpleStringProperty(dCstr);
		return dataCrP;
	}
	
	public String toString(){
		String dCstr=Integer.toString(dataCrearii.get(Calendar.DATE))+" "+Integer.toString(dataCrearii.get(Calendar.MONTH)+1)+" "+Integer.toString(dataCrearii.get(Calendar.YEAR));
		return "ID: "+getId()+ "Titular: "+getTitular().toString()+"\n"+"Sold: "+getSold()+"\n"+ dobanda+"\n"+dCstr+"\n";
	}
}
