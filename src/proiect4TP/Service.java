package proiect4TP;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class Service {

	private static Bank bank=new Bank();
	
	public static void addNewPerson(String id, String nume){
		Person person=new Person(nume, id);
		bank.addPerson(person);
	}
	
	public static void deletePerson(String id,String nume){
		Person person=new Person(nume, id);
		bank.removePerson(person);
	}
	
	public static List<Person> viewAllPersons(){
		List<Person> allPersons=bank.allPersons();
		return allPersons;
	}
	
	public static void addNewSpendingAcc(Person person){
		int idCont=person.getNrConturi()+1;
		Account account=new SpendingAccount(person,0,Integer.toString(idCont));
		bank.addAccount(person, account);
	}
	
	public static void retrageSpending(Person person, String idCont, String sumaString){
		try{
			int suma=Integer.parseInt(sumaString);
			SpendingAccount account=new SpendingAccount(person,0,idCont);
			bank.retrageSpending(person, account, suma);
		}catch(NumberFormatException e){
			System.out.println("Da de ce bagi prostii?..");
		}
	}
	
	public static void depuneSpending(Person person, String idCont, String sumaString){
		try{
			int suma=Integer.parseInt(sumaString);
			SpendingAccount account=new SpendingAccount(person,0,idCont);
			bank.depuneSpending(person, account, suma);
		}catch(NumberFormatException e){
			System.out.println("Da de ce bagi prostii?..");
		}
	}
	
	public static void deleteAll(Person person){
		bank.removeAccounts(person);
	}
	
	public static void deleteSpendingAccount(Person person, String idCont){
		Account account=new SpendingAccount(person,0,idCont);
		bank.removeAccount(person, account);
	}
	
	public static List<Account> viewAllAccounts(){
		List<Account> allAccounts=bank.allAccounts();
		return allAccounts;
	}
	
	public static void addNewSavingAcc(Person person, String dobandaString, String dataCreariiString){
		try{
			int idCont=person.getNrConturi()+1;
			double dobanda=Double.parseDouble(dobandaString);
			DateFormat df=new SimpleDateFormat("dd MM yyyy");
			
			Date date=df.parse(dataCreariiString);
			Calendar dataCrearii=new GregorianCalendar();
			dataCrearii.setTime(date);
			Account account=new SavingAccount(person,0,Integer.toString(idCont),dobanda,dataCrearii);
			bank.addAccount(person, account);
		}catch(NumberFormatException e){
			System.out.println("Da de ce bagi prostii?..");
		} catch (ParseException e) {
			System.out.println("Da de ce bagi prostii?..");
		}
		
	}
	
	public static void deleteSavingAccount(Person person, String idCont){
		
		Account account=new SavingAccount(person,idCont);
		bank.removeAccount(person, account);
	}
	
	public static void retrageSaving(Person person, String idString,String dataString){
		
		try{
			
			DateFormat df=new SimpleDateFormat("dd MM yyyy");
			
			Date date=df.parse(dataString);
			Calendar data=new GregorianCalendar();
			data.setTime(date);
			Account account=new SavingAccount(person,idString);
			bank.retrageSaving(person,account,data);
			
		} catch (ParseException e) {
			System.out.println("Da de ce bagi prostii?..");
		}
	}
	
	public static void depuneSaving(Person person, String idString, String sumaString, String dataString){
		try{
			int suma=Integer.parseInt(sumaString);
			
			DateFormat df=new SimpleDateFormat("dd MM yyyy");
			
			Date date=df.parse(dataString);
			Calendar data=new GregorianCalendar();
			data.setTime(date);
			Account account=new SavingAccount(person,idString);
			bank.depuneSaving(person,account,suma,data);
			
		} catch (ParseException e) {
			System.out.println("Da de ce bagi prostii data?..");
		} catch(NumberFormatException e){
			System.out.println("Da de ce bagi prostii suma?..");
		}
	}
	
	public static void serial(){
		bank.serialize();
	}
	
	public static void deserial(){
		bank.deserialize();
	}
}
