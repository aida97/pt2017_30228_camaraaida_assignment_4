package proiect4TP;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Bank implements BankProc, Serializable{

	
	private static final long serialVersionUID = 1L;
	private HashMap<Person, ArrayList<Account>> banca;
	
	public Bank(){
		banca=new HashMap<Person, ArrayList<Account>>();
	}
	
	public Bank(HashMap<Person, ArrayList<Account>> banca) {
		this.banca = banca;
	}

	public HashMap<Person, ArrayList<Account>> getBanca() {
		return banca;
	}

	public void setBanca(HashMap<Person,ArrayList<Account>> banca) {
		this.banca = banca;
	}

	@Override
	public void addPerson(Person person) {
		// TODO Auto-generated method stub
		assert person!=null;
		assert banca.containsKey(person)==false;
		assert corect()==true;
		int size=banca.size();
		banca.put(person, new ArrayList<Account>());
		
		assert banca.size()==size+1;
		assert banca.containsKey(person)==true;
		assert corect()==true;
		
	}

	@Override
	public void removePerson(Person person) {
		// TODO Auto-generated method stub
		
		assert person!=null;
		assert banca.containsKey(person)==true:"persoana nu exista";
		assert corect()==true;
		int size=banca.size();
		
		banca.remove(person);
			

		assert banca.size()==size-1;
		assert banca.containsKey(person)==false;
		assert corect()==true;
	}

	@Override
	public void addAccount(Person person, Account account) {
		// TODO Auto-generated method stub
		
		assert person!=null;
		assert account!=null;
		assert banca.containsKey(person)==true;
		assert banca.get(person).contains(account)==false;
		assert corect()==true;
		
		ArrayList<Account> conturi=banca.get(person);
		int size=conturi.size();
		int idCont=person.getNrConturi()+1;
		
		
		conturi.add(account);
		banca.put(person, conturi);
		person.setNrConturi(idCont);
		
		assert banca.get(person).size()==size+1;
		assert banca.get(person).contains(account)==true;
		assert corect();
	}

	

	@Override
	public void removeAccounts(Person person) {
		// TODO Auto-generated method stub
		assert person!=null;
		assert banca.containsKey(person)==true;
		assert corect()==true;
		
		ArrayList<Account> conturi=new ArrayList<Account>();
		
		banca.put(person, conturi);
		
		assert banca.get(person).size()==0;
		assert banca.containsKey(person)==true;
		assert corect()==true;
	}

	@Override
	public void removeAccount(Person person, Account account) {
		// TODO Auto-generated method stub
		assert person!=null;
		assert account!=null;
		assert banca.containsKey(person)==true;
		assert banca.get(person).contains(account)==true;
		assert corect()==true;
		
		ArrayList<Account> conturi=banca.get(person);
		int size=conturi.size();
		conturi.remove(account);
		//int nrConturi=person.getNrConturi();
		//person.setNrConturi(nrConturi-1);
		banca.put(person, conturi);
		
		assert banca.get(person).size()==size-1;
		assert banca.get(person).contains(account)==false;
		assert corect();
		
	}

	public void retrageSpending(Person person, Account account, int suma){
		ArrayList<Account> conturi=banca.get(person);
		String idCont=account.getId();
		
		for(int i=0; i<conturi.size(); i++){
			if(conturi.get(i).getId().equals(idCont)){
				((SpendingAccount) conturi.get(i)).retrage(suma);
			}
		}
	}
	
	public void depuneSpending(Person person, Account account, int suma){
		ArrayList<Account> conturi=banca.get(person);
		String idCont=account.getId();
		
		for(int i=0; i<conturi.size(); i++){
			if(conturi.get(i).getId().equals(idCont)){
				((SpendingAccount) conturi.get(i)).depune(suma);
			}
		}
	}
	
	public void retrageSaving(Person person, Account account, Calendar data){
		ArrayList<Account> conturi=banca.get(person);
		String idCont=account.getId();
		
		for(int i=0; i<conturi.size(); i++){
			if(conturi.get(i).getId().equals(idCont)){
				((SavingAccount) conturi.get(i)).retrage(data);
			}
		}
	}
	
	public void depuneSaving(Person person, Account account, int suma, Calendar data){
		ArrayList<Account> conturi=banca.get(person);
		String idCont=account.getId();
		
		for(int i=0; i<conturi.size(); i++){
			if(conturi.get(i).getId().equals(idCont)){
				((SavingAccount) conturi.get(i)).depune(suma, data);
			}
		}
	}
	
	@Override
	public List<Person> allPersons() {
		// TODO Auto-generated method stub
		List<Person> persoane= new ArrayList<Person>();
		Set<Map.Entry<Person,ArrayList<Account>>> set=banca.entrySet();
		
		for(Iterator<Map.Entry<Person,ArrayList<Account>>> it=set.iterator(); it.hasNext();){
			
			Map.Entry<Person, ArrayList<Account>> mapEntry=(Entry<Person,ArrayList<Account>>) it.next();
			Person person=mapEntry.getKey();
			
			persoane.add(person);
		}
		return persoane;
	}

	@Override
	public List<Account> allAccounts() {
		// TODO Auto-generated method stub
		List<Account> allConturi= new ArrayList<Account>();
		Set<Map.Entry<Person,ArrayList<Account>>> set=banca.entrySet();
		
		for(Iterator<Map.Entry<Person,ArrayList<Account>>> it=set.iterator(); it.hasNext();){
			
			Map.Entry<Person, ArrayList<Account>> mapEntry=(Entry<Person,ArrayList<Account>>) it.next();
			
			ArrayList<Account> conturi= mapEntry.getValue();
			for(Account cont: conturi){
				allConturi.add(cont);
			}
			
		}
		
		
		return allConturi;
	}

	

	@Override
	public boolean corect() {
		// TODO Auto-generated method stub
		Set<Map.Entry<Person,ArrayList<Account>>> set=banca.entrySet();
		
		for(Iterator<Map.Entry<Person,ArrayList<Account>>> it=set.iterator(); it.hasNext();){
			
			Map.Entry<Person, ArrayList<Account>> mapEntry=(Entry<Person,ArrayList<Account>>) it.next();
			Person person=mapEntry.getKey();
			ArrayList<Account> conturi= mapEntry.getValue();
			
			if(person==null || conturi==null){
				return false;
			}
		}
		return true;
		
		
	}

	@Override
	public void serialize() {
		
		try {
			FileOutputStream fileOut=new FileOutputStream("banca.ser");
			ObjectOutputStream out=new ObjectOutputStream(fileOut);
			out.writeObject(this);
			out.close();
			fileOut.close();
			System.out.println("Datele serializate sunt scrise in banca.ser");
		} catch (FileNotFoundException e) {
			System.out.println("Nu am gasit fiserul...");
		} catch (IOException e){
			System.out.println("IO Exception...");
		}
	}

	@Override
	public void deserialize() {
		Bank bank=null;
		try {
			FileInputStream fileIn=new FileInputStream("banca.ser");
			ObjectInputStream in=new ObjectInputStream(fileIn);
			bank=(Bank)in.readObject();
		} catch (FileNotFoundException e) {
			System.out.println("Nu am gasit fiserul...");
		} catch (IOException e) {
			System.out.println("IO Exception...");
		} catch (ClassNotFoundException e) {
			System.out.println("Class not found exception...");
		}
		
		Set<Map.Entry<Person,ArrayList<Account>>> set=bank.banca.entrySet();
		for(Iterator<Map.Entry<Person,ArrayList<Account>>> it=set.iterator(); it.hasNext();){
			
			Map.Entry<Person, ArrayList<Account>> mapEntry=(Entry<Person,ArrayList<Account>>) it.next();
			Person person=mapEntry.getKey();
			
			ArrayList<Account> conturi= mapEntry.getValue();
			this.banca.put(person, conturi);
		}
	}

}
