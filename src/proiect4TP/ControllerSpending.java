package proiect4TP;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;

public class ControllerSpending {

	@FXML
	private TextField sumaDepusa;
	
	@FXML
	private TextField sumaRetrasa;
	
	@FXML
	private ComboBox<Person> persoaneCB;
	
	@FXML
	private TextField idCont;
	
	
	public ControllerSpending() {

	}

	@FXML
	private void initialize() {
		
	}
	
	@FXML
	private void setComboBox(){
		List<Person> persoane=Service.viewAllPersons();
		
        List<Person> persoaneAL=new ArrayList<Person>();
        for(Person persoana: persoane){
			persoaneAL.add(persoana);
		}
        
        ObservableList<Person> data = FXCollections.observableList(persoaneAL);
        
        persoaneCB.setEditable(false);
        persoaneCB.setItems(data);
	}
	
	public void addNew(){
		Person person=persoaneCB.getValue();
		Service.addNewSpendingAcc(person);
	}
	
	public void deleteAll(){
		Person person=persoaneCB.getValue();
		Service.deleteAll(person);
	}
	
	public void deleteAccount(){
		Person person=persoaneCB.getValue();
		String idContString=idCont.getText();
		Service.deleteSpendingAccount(person,idContString);

	}
	public void retrage(){
		Person person=persoaneCB.getValue();
		String sumaString=sumaRetrasa.getText();
		String idString=idCont.getText();
		Service.retrageSpending(person, idString, sumaString);
	}
	
	public void depune(){
		Person person=persoaneCB.getValue();
		String sumaString=sumaDepusa.getText();
		String idString=idCont.getText();
		Service.depuneSpending(person, idString, sumaString);
	}
	
	public void viewAll(){
		Stage stage = new Stage();
		Group root=new Group();
		Scene scene = new Scene(root);
        stage.setTitle("Conturi");
        stage.setWidth(600);
        stage.setHeight(500);
 
        GridPane panel=new GridPane();
        List<Account> conturi=Service.viewAllAccounts();
        List<Account> conturiAL=new ArrayList<Account>();
        for(Account cont: conturi){
			conturiAL.add(cont);
		}
        
         
        
        ObservableList<Account> data = FXCollections.observableList(conturiAL);
        TableView<Account> table = new TableView<Account>();
        table.setMinWidth(500);
        table.setItems(data);
     
        TableColumn<Account,String> numeTitularCol=new TableColumn<Account,String>("Nume Titular");
        TableColumn<Account,String> idTitularCol=new TableColumn<Account,String>("ID Titular");
        
        TableColumn<Account,String> idContCol=new TableColumn<Account,String>("ID Cont");
        TableColumn<Account,String> soldContCol=new TableColumn<Account,String>("Sold Cont");
        
        TableColumn<Account,String> tipContCol=new TableColumn<Account,String>("Tip Cont");
        
        numeTitularCol.setCellValueFactory(new Callback<CellDataFeatures<Account, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(CellDataFeatures<Account, String> p) {
                // p.getValue() returns the Person instance for a particular TableView row
                return p.getValue().getNumeP();
            }
         });
        
        idTitularCol.setCellValueFactory(new Callback<CellDataFeatures<Account, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(CellDataFeatures<Account, String> p) {
                // p.getValue() returns the Person instance for a particular TableView row
                return p.getValue().getIdTP();
            }
         });
        
        idContCol.setCellValueFactory(new Callback<CellDataFeatures<Account, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(CellDataFeatures<Account, String> p) {
                // p.getValue() returns the Person instance for a particular TableView row
                return p.getValue().getIdP();
            }
         });
        
        soldContCol.setCellValueFactory(new Callback<CellDataFeatures<Account, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(CellDataFeatures<Account, String> p) {
                // p.getValue() returns the Person instance for a particular TableView row
                return p.getValue().getSoldP();
            }
         });

        tipContCol.setCellValueFactory(new Callback<CellDataFeatures<Account, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(CellDataFeatures<Account, String> p) {
                // p.getValue() returns the Person instance for a particular TableView row
            	
            	SimpleStringProperty tipCont=new SimpleStringProperty(p.getValue().getClass().getName());
        		return tipCont;
            }
         });

	table.getColumns().addAll(numeTitularCol,idTitularCol,idContCol, soldContCol, tipContCol);

	root.getChildren().add(panel);
        root.getChildren().addAll(table);
        
        
        stage.setScene(scene);
        stage.show();
	}
	
}
