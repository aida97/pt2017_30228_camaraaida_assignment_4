package proiect4TP;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Observable;

import javafx.beans.property.SimpleStringProperty;

public abstract class Account extends Observable implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Person titular;
	private double sold;
	private String id;
	
	public Account(){
		titular=new Person();
		sold=0;
		id=Integer.toString(titular.getNrConturi());
		this.addObserver(titular);
	}

	public Account(Person titular, double sold,String id) {
		this.titular = titular;
		this.sold = sold;
		this.id=id;
		this.addObserver(this.titular);
	}
	
	public Account(Person titular,String id){
		this.titular = titular;
		this.id=id;
		this.addObserver(this.titular);
	}
	public Person getTitular() {
		return titular;
	}

	public void setTitular(Person titular) {
		this.titular = titular;
	}

	public double getSold() {
		return sold;
	}

	public void setSold(int sold) {
		this.sold = sold;
		setChanged();
	    notifyObservers();

	}
	
	public void setSold(double sold) {
		this.sold = sold;
		setChanged();
	    notifyObservers();

	}
	
	public String getId(){
		return id;
	}
	
	public void setId(String id){
		this.id=id;
	}

	
	
	
	public SimpleStringProperty getNumeP(){
		return titular.getNumeP();
	}
	
	public SimpleStringProperty getIdTP(){
		return titular.getIdP();
	}
	
	public SimpleStringProperty getSoldP(){
		SimpleStringProperty soldP=new SimpleStringProperty(Double.toString(sold));
		return soldP;
	}
	
	public SimpleStringProperty getIdP(){
		SimpleStringProperty idP=new SimpleStringProperty(id);
		return idP;
	}
	
	
	//public abstract void retrage(int suma);
	//public abstract void retrage(int suma, Calendar data);
	//public abstract void depune(int suma);
	
	public boolean equals(Object oth) {
		Account other=(Account) oth;
		if(other.getId().equals(this.getId())){
			return true;
		}
		return false;
	}

}
