package proiect4TP;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Controller {

	public Controller() {

	}

	@FXML
	private void initialize() {
		
	}
	@FXML
	public void personNewWindow(){
		FXMLLoader loader = new FXMLLoader();
		Stage stage = new Stage();

		String fxmlDocPath = "C:/Users/Aida/Desktop/fxmlfiles/personOperatii.fxml";
		FileInputStream fxmlStream;
		try {
			fxmlStream = new FileInputStream(fxmlDocPath);
			AnchorPane root = (AnchorPane) loader.load(fxmlStream);
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.setTitle("proiect4");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}
		
		stage.show();
	}
	
	@FXML
	public void accountNewWindow(){
		FXMLLoader loader = new FXMLLoader();
		Stage stage = new Stage();

		String fxmlDocPath = "C:/Users/Aida/Desktop/fxmlfiles/selAccount.fxml";
		FileInputStream fxmlStream;
		try {
			fxmlStream = new FileInputStream(fxmlDocPath);
			AnchorPane root = (AnchorPane) loader.load(fxmlStream);
			Scene scene = new Scene(root);
			stage.setScene(scene);
			stage.setTitle("proiect4");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}
		
		stage.show();
	}
	
	public void serial(){
		Service.serial();
	}
	
	public void deserial(){
		Service.deserial();
	}
}
