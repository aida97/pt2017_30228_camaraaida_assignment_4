package proiect4TP;

import java.util.ArrayList;
import java.util.List;


import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;

public class ControllerPerson {

	@FXML
	private TextField id;
	
	@FXML
	private TextField nume;
	
	
	public ControllerPerson() {

	}

	@FXML
	private void initialize() {
		
	}
	
	public void addNew(){
		String idString=id.getText();
		String numeString=nume.getText();
		
		Service.addNewPerson(idString,numeString);
	}
	
	public void delete(){
		String idString=id.getText();
		String numeString=nume.getText();
		Service.deletePerson(idString,numeString);
	}
	
	public void viewAll(){
		
		Stage stage = new Stage();
		Group root=new Group();
		Scene scene = new Scene(root);
        stage.setTitle("Persoane");
        stage.setWidth(300);
        stage.setHeight(500);
 
        GridPane panel=new GridPane();
        List<Person> persoane=Service.viewAllPersons();
        List<Person> persoaneAL=new ArrayList<Person>();
        for(Person persoana: persoane){
			persoaneAL.add(persoana);
		}
        
         
        
        ObservableList<Person> data = FXCollections.observableList(persoaneAL);
        TableView<Person> table = new TableView<Person>();
        table.setItems(data);
     
        TableColumn<Person,String> numeCol=new TableColumn<Person,String>("Nume");
        TableColumn<Person,String> idCol=new TableColumn<Person,String>("Id");
       
        
        
        numeCol.setCellValueFactory(new Callback<CellDataFeatures<Person, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(CellDataFeatures<Person, String> p) {
                // p.getValue() returns the Person instance for a particular TableView row
                return p.getValue().getNumeP();
            }
         });
        
        idCol.setCellValueFactory(new Callback<CellDataFeatures<Person, String>, ObservableValue<String>>() {
            public ObservableValue<String> call(CellDataFeatures<Person, String> p) {
                // p.getValue() returns the Person instance for a particular TableView row
                return p.getValue().getIdP();
            }
         });

	table.getColumns().addAll(idCol,numeCol);

	root.getChildren().add(panel);
        root.getChildren().addAll(table);
        
        
        stage.setScene(scene);
        stage.show();
		
		
	}
}
