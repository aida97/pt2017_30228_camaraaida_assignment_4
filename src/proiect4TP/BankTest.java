package proiect4TP;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;



public class BankTest {

	private Bank bank;
	@Before
	public void setUpBeforeClass() throws Exception {

		bank = new Bank();
	
	}
	
	@Test
	public void testAdaugaPersoana() {
		Person person=new Person("Bianca", "113");
		bank.addPerson(person);
		
		assertEquals(true, bank.getBanca().containsKey(person));
	}
	
	@Test
	public void testAdaugaCont(){
		Person person=new Person("Bianca", "113");
		Account cont=new SpendingAccount(person,7,"1");
		bank.addPerson(person);
		bank.addAccount(person, cont);
		assertEquals(true, bank.getBanca().get(person).contains(cont));
	}
	
	@Test
	public void testRetrageSpending(){
		Person person=new Person("Bianca", "113");
		Account cont=new SpendingAccount(person,7,"1");
		bank.addPerson(person);
		bank.addAccount(person, cont);
		bank.retrageSpending(person, cont, 3);
		assertEquals(true,  bank.getBanca().get(person).get(0).getSold()==4);
	}
	
	@Test
	public void testStergeCont(){
		Person person=new Person("Bianca", "113");
		Account cont=new SpendingAccount(person,7,"1");
		bank.addPerson(person);
		bank.addAccount(person, cont);
		bank.removeAccount(person, cont);
		assertEquals(false, bank.getBanca().get(person).contains(cont));
	}

}
