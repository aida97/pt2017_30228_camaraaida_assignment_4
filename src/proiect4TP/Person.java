package proiect4TP;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

import javafx.beans.property.SimpleStringProperty;

public class Person implements Serializable, Observer {
	
	private static final long serialVersionUID = 1L;
	private String nume;
	private String id;
	private int nrConturi;
	
	public Person(){
		nume="nimic";
		id=" ";
		nrConturi=0;
	}
	

	public Person(String nume, String iD) {
		this.nume = nume;
		id = iD;
		this.nrConturi=0;
	}


	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getId() {
		return id;
	}

	public void setId(String iD) {
		id = iD;
	}
	
	public int getNrConturi(){
		return nrConturi;
	}
	public void setNrConturi(int nr){
		nrConturi=nr;
	}
	
	public SimpleStringProperty getNumeP(){
		SimpleStringProperty numeP=new SimpleStringProperty(nume);
		return numeP;
	}
	
	public SimpleStringProperty getIdP(){
		SimpleStringProperty idP=new SimpleStringProperty(id);
		return idP;
	}
	
	@Override
	public boolean equals(Object oth){
		if(oth instanceof String){
		
			Person other=fromString((String)oth);
			if(other.getId().equals(id )){
				return true;
			}
			return false;
		}else{
			if(oth==null)
				return false;
			Person other=(Person) oth;
			//System.out.println(other);
			if(other.getId().equals(id )){
				return true;
			}
			return false;
		}
		
	}
	
	/*
	public boolean equals(String oth){
		Person other=fromString(oth);
		if(other.getId().equals(id )){
			return true;
		}
		return false;
	}
	
	public boolean equals(Person other){
		
		if(other.getId().equals(id )){
			return true;
		}
		return false;
	}
*/
	@Override
	public int hashCode(){
		int code=Integer.parseInt(id);
		int sum=0;
		while(code!=0){
			sum=code%10;
			code=code/10;
		}
		return sum%1000;
	}
	
	@Override
	public void update(Observable observable, Object obj) {
		// TODO Auto-generated method stub
		System.out.println("Ma numesc: "+nume+" si mi s-a schimbat un cont");
	}

	public String toString(){
		return id+" "+nume;
	}
	
	 public Person fromString(String string) {
      
     	int spatiu=string.indexOf(' ');
     	String id=string.substring(0, spatiu);
     	String nume=string.substring(spatiu+1);
         return new Person(nume, id);
     }

}
